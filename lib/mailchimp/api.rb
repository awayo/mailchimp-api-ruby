module Mailchimp

    class Lists
        attr_accessor :master

        def initialize(master)
            @master = master
        end

        def list(fields=[], exclude_fields=[], start=0, limit=25, before_date_created=nil, before_campaings_last_send=nil, since_campaings_last_send=nil, since_date_created=nil)
            _params = {:fields => fields, :exclude_fields => exclude_fields, :offset => start, :count => limit}
            _params.merge!({:before_date_created => before_date_created}) unless before_date_created.nil?
            _params.merge!({:before_campaings_last_send => before_campaings_last_send}) unless before_campaings_last_send.nil?
            _params.merge!({:since_campaings_last_send => since_campaings_last_send}) unless since_campaings_last_send.nil?
            _params.merge!({:since_date_created => since_date_created}) unless since_date_created.nil?
            return @master.list 'lists', _params
        end

        def get(lid, fields=[], exclude_fields=[])
            _params = {:fields => fields, :exclude_fields => exclude_fields}
            return @master.list "lists/#{lid}", _params
        end

        def edit(lid, fields=[])
            _params = {:fields => fields}
            return @master.edit "lists/#{lid}", fields
        end

        def dup(lid, permission_reminder, visibility = 'pub')
            response = get(lid)

            puts response if @debug

            _params = {
                :name => response['name'],
                :contact => response['contact'],
                :permission_reminder => permission_reminder,
                :use_archive_bar => response['use_archive_bar'],
                :campaign_defaults => response['campaign_defaults'],
                :email_type_option => response['email_type_option'],
                :visibility => visibility

            }
            @master.create 'lists', _params
        end

      def add_member(lid, email, status='subscribed')
          _params={:email_address => email, :status => status}
          @master.create "lists/#{lid}/members", _params
      end

      def add_subscribed_members(lid, emails=[])
          operations = []
          emails.each do |email|
              operations << {:method => 'POST', :path => "lists/#{lid}/members", :body => JSON.generate({:email_address => email, :status => 'subscribed'})}
          end

        @master.create 'batches', {:operations => operations}
      end
    end

    class Templates
        attr_accessor :master

        def initialize(master)
            @master = master
        end

        def list(fields=[], exclude_fields=[], start=0, limit=25, before_created_at=nil, created_by=nil, type=nil, since_created_at=nil)
            _params = {:fields => fields, :exclude_fields => exclude_fields, :offset => start, :count => limit}
            _params.merge!({:before_created_at => before_created_at}) unless before_created_at.nil?
            _params.merge!({:created_by => created_by}) unless created_by.nil?
            _params.merge!({:type => type}) unless type.nil?
            _params.merge!({:since_created_at => since_created_at}) unless since_created_at.nil?
            return @master.list 'templates', _params
        end

        def get(tid)
            return @master.list "templates/#{tid}"
        end
    end

    class Campaigns
        attr_accessor :master

        def initialize(master)
            @master = master
        end

        # Get the content (both html and text) for a campaign either as it would appear in the campaign archive or as the raw, original content
        # @param [String] cid the campaign id to get content for (can be gathered using campaigns/list())
        # @param [Hash] options various options to control this list
        #     - [String] view optional one of "archive" (default), "preview" (like our popup-preview) or "raw"
        #     - [Hash] email optional if provided, view is "archive" or "preview", the campaign's list still exists, and the requested record is subscribed to the list. the returned content will be populated with member data populated. a struct with one of the following keys - failing to provide anything will produce an error relating to the email address. If multiple keys are provided, the first one from the following list that we find will be used, the rest will be ignored.
        #         - [String] email an email address
        #         - [String] euid the unique id for an email address (not list related) - the email "id" returned from listMemberInfo, Webhooks, Campaigns, etc.
        #         - [String] leid the list email id (previously listed web_id) for a list-member-info type list. this doesn't change when the email address changes
        # @return [Hash] containing all content for the campaign
        #     - [String] html The HTML content used for the campaign with merge tags intact
        #     - [String] text The Text content used for the campaign with merge tags intact
        def content(cid, options=[])
            _params = {:options => options}
            return @master.list "campaigns/#{cid}/content", _params
        end

        # Get the content (both html and text) for a campaign either as it would appear in the campaign archive or as the raw, original content
        # @param [String] cid the campaign id to get content for (can be gathered using campaigns/list())
        # @param [String] plain_text email plain text
        # @param [String] html email raw html content
        # @param [String] url	When importing a campaign, the URL where the HTML lives.
        # @param [Hash] template
        #   - id
        #   - sections
        # @param [Hash] archive
        #   - archive_content
        #   - archive_type
        # @param [Hash] variate_content
        #   - content_label
        #   - plain_text
        #   - html
        #   - url
        #   - template
        #   - archive
         # @return [Hash] containing all content for the campaign
        #     - [String] html The HTML content used for the campaign with merge tags intact
        #     - [String] text The Text content used for the campaign with merge tags intact
        def edit_content(cid, plain_text, html, url, template, archive, variate_contents)
            _params = {}

            _params.merge!({:plain_text => plain_text}) unless plain_text.nil?
            _params.merge!({:html => html}) unless html.nil?
            _params.merge!({:url => url}) unless url.nil?
            _params.merge!({:template => template}) unless template.nil?
            _params.merge!({:archive => archive}) unless archive.nil?
            _params.merge!({:variate_contents => variate_contents}) unless variate_contents.nil?

            return @master.set "campaigns/#{cid}/content", _params
        end


        # Create a new draft campaign to send. You <strong>can not</strong> have more than 32,000 campaigns in your account.
        # @param [String] type the Campaign Type to create - one of "regular", "plaintext", "absplit", "rss", "auto"
        # @param [Hash] settings The settings for your campaign, including subject, from name, reply-to address, and more.
        # @param [Hash] recipents List settings for the campaign.
        # @return [Hash] the new campaign's details - will return same data as single campaign from campaigns/list()
        def create(type, settings, recipents, social_card = {})
            _params = {:type => type, :recipients => recipents, :settings => settings}
            _params = _params.merge({:social_card => social_card}) unless social_card.size == 0

            return @master.create 'campaigns', _params
        end

        # Delete a campaign. Seriously, "poof, gone!" - be careful! Seriously, no one can undelete these.
        # @param [String] cid the Campaign Id to delete
        # @return [Hash] with a single entry:
        #     - [Bool] complete whether the list worked. reallistilisty this will always be true as errors will be thrown otherwise.
        def delete(cid)
            return @master.delete "campaigns/#{cid}"
        end

        # Get the list of campaigns and their details matching the specified filters
        # @param [Hash] filters a struct of filters to apply to this query - all are optional:
        #     - [String] campaign_id optional - return the campaign using a know campaign_id.  Accepts multiples separated by commas when not using exact matching.
        #     - [String] parent_id optional - return the child campaigns using a known parent campaign_id.  Accepts multiples separated by commas when not using exact matching.
        #     - [String] list_id optional - the list to send this campaign to - get lists using lists/list(). Accepts multiples separated by commas when not using exact matching.
        #     - [Int] folder_id optional - only show campaigns from this folder id - get folders using folders/list(). Accepts multiples separated by commas when not using exact matching.
        #     - [Int] template_id optional - only show campaigns using this template id - get templates using templates/list(). Accepts multiples separated by commas when not using exact matching.
        #     - [String] status optional - return campaigns of a specific status - one of "sent", "save", "paused", "schedule", "sending". Accepts multiples separated by commas when not using exact matching.
        #     - [String] type optional - return campaigns of a specific type - one of "regular", "plaintext", "absplit", "rss", "auto". Accepts multiples separated by commas when not using exact matching.
        #     - [String] from_name optional - only show campaigns that have this "From Name"
        #     - [String] from_email optional - only show campaigns that have this "Reply-to Email"
        #     - [String] title optional - only show campaigns that have this title
        #     - [String] subject optional - only show campaigns that have this subject
        #     - [String] sendtime_start optional - only show campaigns that have been sent since this date/time (in GMT) -  - 24 hour format in <strong>GMT</strong>, eg "2013-12-30 20:30:00" - if this is invalid the whole list fails
        #     - [String] sendtime_end optional - only show campaigns that have been sent before this date/time (in GMT) -  - 24 hour format in <strong>GMT</strong>, eg "2013-12-30 20:30:00" - if this is invalid the whole list fails
        #     - [Boolean] uses_segment - whether to return just campaigns with or without segments
        #     - [Boolean] exact optional - flag for whether to filter on exact values when filtering, or search within content for filter values - defaults to true. Using this disables the use of any filters that accept multiples.
        # @param [Int] start optional - control paging of campaigns, start results at this campaign #, defaults to 1st page of data  (page 0)
        # @param [Int] limit optional - control paging of campaigns, number of campaigns to return with each list, defaults to 25 (max=1000)
        # @param [String] sort_field optional - one of "create_time", "send_time", "title", "subject" . Invalid values will fall back on "create_time" - case insensitive.
        # @param [String] sort_dir optional - "DESC" for descending (default), "ASC" for Ascending.  Invalid values will fall back on "DESC" - case insensitive.
        # @return [Hash] containing a count of all matching campaigns, the specific ones for the current page, and any errors from the filters provided
        #     - [Int] total the total number of campaigns matching the filters passed in
        #     - [Array] data structs for each campaign being returned
        #         - [String] id Campaign Id (used for all other campaign functions)
        #         - [Int] web_id The Campaign id used in our web app, allows you to create a link directly to it
        #         - [String] list_id The List used for this campaign
        #         - [Int] folder_id The Folder this campaign is in
        #         - [Int] template_id The Template this campaign uses
        #         - [String] content_type How the campaign's content is put together - one of 'template', 'html', 'url'
        #         - [String] title Title of the campaign
        #         - [String] type The type of campaign this is (regular,plaintext,absplit,rss,inspection,auto)
        #         - [String] create_time Creation time for the campaign
        #         - [String] send_time Send time for the campaign - also the scheduled time for scheduled campaigns.
        #         - [Int] emails_sent Number of emails email was sent to
        #         - [String] status Status of the given campaign (save,paused,schedule,sending,sent)
        #         - [String] from_name From name of the given campaign
        #         - [String] from_email Reply-to email of the given campaign
        #         - [String] subject Subject of the given campaign
        #         - [String] to_name Custom "To:" email string using merge variables
        #         - [String] archive_url Archive link for the given campaign
        #         - [Boolean] inline_css Whether or not the campaign content's css was auto-inlined
        #         - [String] analytics Either "google" if enabled or "N" if disabled
        #         - [String] analytics_tag The name/tag the campaign's links were tagged with if analytics were enabled.
        #         - [Boolean] authenticate Whether or not the campaign was authenticated
        #         - [Boolean] ecomm360 Whether or not ecomm360 tracking was appended to links
        #         - [Boolean] auto_tweet Whether or not the campaign was auto tweeted after sending
        #         - [String] auto_fb_post A comma delimited list of Facebook Profile/Page Ids the campaign was posted to after sending. If not used, blank.
        #         - [Boolean] auto_footer Whether or not the auto_footer was manually turned on
        #         - [Boolean] timewarp Whether or not the campaign used Timewarp
        #         - [String] timewarp_schedule The time, in GMT, that the Timewarp campaign is being sent. For A/B Split campaigns, this is blank and is instead in their schedule_a and schedule_b in the type_opts array
        #         - [String] parent_id the unique id of the parent campaign (currently only valid for rss children). Will be blank for non-rss child campaigns or parent campaign has been deleted.
        #         - [Boolean] is_child true if this is an RSS child campaign. Will return true even if the parent campaign has been deleted.
        #         - [String] tests_sent tests sent
        #         - [Int] tests_remain test sends remaining
        #         - [Hash] tracking the various tracking options used
        #             - [Boolean] html_clicks whether or not tracking for html clicks was enabled.
        #             - [Boolean] text_clicks whether or not tracking for text clicks was enabled.
        #             - [Boolean] opens whether or not opens tracking was enabled.
        #         - [String] segment_text a string marked-up with HTML explaining the segment used for the campaign in plain English
        #         - [Array] segment_opts the segment used for the campaign - can be passed to campaigns/segment-test or campaigns/create()
        #         - [Hash] saved_segment if a saved segment was used (match+conditions returned above):
        #             - [Int] id the saved segment id
        #             - [String] type the saved segment type
        #             - [String] name the saved segment name
        #         - [Hash] type_opts the type-specific options for the campaign - can be passed to campaigns/create()
        #         - [Int] comments_total total number of comments left on this campaign
        #         - [Int] comments_unread total number of unread comments for this campaign based on the login the apikey belongs to
        #         - [Hash] summary if available, the basic aggregate stats returned by reports/summary
        #         - [Hash] social_card If a social card has been attached to this campaign:
        #             - [String] title The title of the campaign used with the card
        #             - [String] description The description used with the card
        #             - [String] image_url The URL of the image used with the card
        #             - [String] enabled Whether or not the social card is enabled for this campaign.
        #     - [Array] errors structs of any errors found while loading lists - usually just from providing invalid list ids
        #         - [String] filter the filter that caused the failure
        #         - [String] value the filter value that caused the failure
        #         - [Int] code the error code
        #         - [String] error the error message
        def list(filters=[], start=0, limit=25, sort_field='create_time', sort_dir='DESC')
            _params = {:filters => filters, :start => start, :limit => limit, :sort_field => sort_field, :sort_dir => sort_dir}
            return @master.list 'campaigns', _params
        end


        # Schedule a campaign to be sent in the future
        # @param [String] cid the id of the campaign to schedule
        # @param [String] schedule_time the time to schedule the campaign. For A/B Split "schedule" campaigns, the time for Group A - 24 hour format in <strong>GMT</strong>, eg "2013-12-30 20:30:00"
        # @param [String] timewarp send the email based on email tz
        # @return [Hash] with a single entry:
        #     - [Bool] complete whether the list worked. reallistilisty this will always be true as errors will be thrown otherwise.
        def schedule(cid, schedule_time, timewarp=true)
            _params = {:schedule_time => schedule_time, :timewarp => timewarp}
            return @master.create "campaigns/#{cid}/actions/schedule", _params
        end

        # Allows one to test their segmentation rules before creating a campaign using them.
        # @param [String] test_emails list of emails
        # @param [Hash] send_type: ['hmtl','plain_text']
        # @return [Hash] with a single entry:
        #     - [Int] total The total number of subscribers matching your segmentation options
        def test(cid, test_emails, send_type = 'html')
            _params = {:test_emails => test_emails, :send_type => send_type}
            return @master.create "campaigns/#{cid}/actions/test", _params
        end

        # Unschedule a campaign that is scheduled to be sent in the future
        # @param [String] cid the id of the campaign to unschedule
        # @return [Hash] with a single entry:
        #     - [Bool] complete whether the list worked. reallistilisty this will always be true as errors will be thrown otherwise.
        def unschedule(cid)
            _params = {}
            return @master.create "campaigns/#{cid}/actions/unschedule", _params
        end

        # Send a campaign right now
        # @param [String] cid the id of the campaign to send
        # @return [Hash] with a single entry:
        #     - [Bool] complete whether the list worked. reallistilisty this will always be true as errors will be thrown otherwise.
        def send(cid)
            _params = {}
            return @master.create "campaigns/#{cid}/actions/send", _params
        end

        # Update just about any setting besides type for a campaign that has <em>not</em> been sent. See campaigns/create() for details. Caveats:<br/><ul class='bullets'> <li>If you set a new list_id, all segmentation options will be deleted and must be re-added.</li> <li>If you set template_id, you need to follow that up by setting it's 'content'</li> <li>If you set segment_opts, you should have tested your options against campaigns/segment-test().</li> <li>To clear/unset segment_opts, pass an empty string or array as the value. Various wrappers may require one or the other.</li> </ul>
        # @param [String] cid the Campaign Id to update
        # @param [Hash] settings The settings for your campaign, including subject, from name, reply-to address, and more.
        # @param [Hash] recipents List settings for the campaign.
        # @return [Hash] updated campaign details and any errors
        #     - [Hash] data the update campaign details - will return same data as single campaign from campaigns/list()
        #     - [Array] errors for "options" only - structs containing:
        #         - [Int] code the error code
        #         - [String] message the full error message
        #         - [String] name the parameter name that failed
        def update(type, settings, recipents, social_card = {})
        _params = {:type => type, :recipents => recipents, :settings => settings}
        _params = _params.merge({:social_card => social_card}) unless social_card.size == 0
            return @master.modify 'campaigns', _params
        end

    end

end

