Overview
=============================================
A Ruby API client for [v3 of the MailChimp API](http://apidocs.mailchimp.com/api/3.0/). Please note that we generate this client/wrapper, so while we're happy to look at any pull requests, ultimately we can't technically accept them. We will, however comment on any additions or changes made due to them before closing them.


###Usage
This gem is only on this repository, sorry folks if you want to try it use:
```
gem 'mailchimp-api', require: 'mailchimp', :git => 'https://bitbucket.org/awayo/mailchimp-api-ruby.git'
```
In your Gemfile and then `bundle install` and you're all set.

But i recommend you that made a fork of this repo.

Example
---

A basic example app to help kickstart you which - will also automatically install this package - is over in a public [github](https://github.com/mailchimp/mcapi2-ruby-examples) repo

Make the following constants, in your environment file or where ever you are mentioning the constants.


    MAILCHIMP-API-KEY = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-xxx"


You can have a look [here][4] (mailchimp documentation), how you can get the API-KEY.

Now there are multiple things that you can do from mailchimp-api. I am mentioning the basic's that used commonly.

Now create the List at mailchimp and [get the listId][5].

Add it in your constants, where you mentioned the API-KEY.


MAILCHIMP-LIST-ID = "xxxxxxxxxx"

Now the time is to get to know how to use [mailchimp-api gem][6]

UseCases
--------

TODO all methods changed in this version.